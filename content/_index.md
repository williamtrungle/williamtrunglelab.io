---
title: William Trung Le
description: Research associate in deep rad-onc
contact: "@williamtrungle"
experience:
- CHUM
- Accedian
- Bell Canada
- École d'optométrie
images:
- roselys
- bujo
- plant
---
I'm a research associate at the CHUM Research Center based in Montreal, Canada. My work consists of appying deep learning techniques to solve medical imaging problems in radio-oncology, specifically for cancer risk assessment and radiotherapy planning.

Having graduated in both biomedical sciences and computer sciences, I try to combine good software engineering practices with standard research methodology allowing reproducibility and more efficient computing resources management.

Outside of work, my hobbies range between programming language design, fitness, karate, bullet journaling and fountain pens, readding fiction, being a local foodie, photography, web design, plants, and cinema.
